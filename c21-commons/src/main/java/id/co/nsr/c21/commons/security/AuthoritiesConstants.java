package id.co.nsr.c21.commons.security;

/**
 * Constants for Spring Security authorities.
 */
public class AuthoritiesConstants {
	
    public static final String ROLE_ADMIN = "ROLE_ADMIN";

    public static final String ROLE_USER = "ROLE_USER";

    public static final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";

    private AuthoritiesConstants() {
    }

}
