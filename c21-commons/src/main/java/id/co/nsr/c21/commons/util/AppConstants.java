package id.co.nsr.c21.commons.util;

public interface AppConstants {

    String SPRING_PROFILE_DEVELOPMENT = "dev";
    String SPRING_PROFILE_SIT = "sit";
    String SPRING_PROFILE_PRODUCTION = "prod";
    
    public static final String ACCOUNT_SYSTEM = "system";
    public static final String ACCOUNT_ANONYMOUS = "anonymoususer";
	
}
